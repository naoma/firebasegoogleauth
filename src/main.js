import Vue from "vue";
import App from "./App.vue";
// 追加ここから ---
import firebase from 'firebase/app';
import 'firebase/auth';
Vue.config.productionTip = false;

const config = {
  apiKey: "AIzaSyAVlZDmk3_wKGKDlr8FL9Q4HHwdRsEOIks",
  authDomain: "fire-base-auth-test-e3b61.firebaseapp.com",
  databaseURL: "https://fire-base-auth-test-e3b61.firebaseio.com",
  projectId: "fire-base-auth-test-e3b61",
  storageBucket: "fire-base-auth-test-e3b61.appspot.com",
  messagingSenderId: "482719146627"
};
firebase.initializeApp(config);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount("#app");
